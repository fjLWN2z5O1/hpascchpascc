##project base

https://www.kaggle.com/c/hpa-single-cell-image-classification/overview/evaluation

tl;dr

each image has up to 18 potential labels, 0-17, with 18 being the null set

the green bitmap is a mask produced by the evaluation, in addition to labels.

the other 3 bitmaps show cell organelles.

http://cocodataset.org/#download mask api encodes the mask for upstream scoring purposes.


## sample data input
[train](./src/test/resources/train/) and   [test](./src/test/resources/test/) contain two such samples.


## random prep notes

### create a csv of image sizes. 
```bash

 for i in {0..9} {a..f} 
 do
   find -name $i\*_green.png |   
   while read 
    do x=($( ffprobe 2>&1 -hide_banner  $REPLY|tail -n1) )
    echo $(basename ${REPLY//_green.png}),${x[5]} 
    done >sizes.$i.txt & 
 done
 wait
 
 echo uuid,dimensions >sizes.csv 
 sort -fu sizes*txt >>sizes.csv
```

### verify 
`wc -l sizes.*txt`

```
   1340 sizes.0.txt
   1419 sizes.1.txt
   1428 sizes.2.txt
   1322 sizes.3.txt
   1389 sizes.4.txt
   1388 sizes.5.txt
   1415 sizes.6.txt
   1339 sizes.7.txt
   1420 sizes.8.txt
   1386 sizes.9.txt
   1414 sizes.a.txt
   1451 sizes.b.txt
   1388 sizes.c.txt
   1441 sizes.d.txt
   1414 sizes.e.txt
   1411 sizes.f.txt
  22365 total 
```

`wc -l sizes.csv`
22366 sizes.csv


