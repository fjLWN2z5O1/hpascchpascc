import cursors.Cursor
import cursors.at
import cursors.context.Scalar
import cursors.io.IOMemento
import vec.macros.get
import vec.macros.size
import vec.macros.t2
import vec.util.logDebug
import java.awt.image.BufferedImage
import java.io.File
import javax.imageio.ImageIO
import kotlin.coroutines.CoroutineContext
import kotlin.math.min
import kotlin.random.Random


@ExperimentalUnsignedTypes
fun main(args: Array<String>) {
    val fname = if (args.size > 0) args[0] else "src/test/resources/test/0040581b-f1f2-4fbe-b043-b6bfea5404bb_green.png"

    val tmpbmp = File.createTempFile("hpascc", ".png")
    tmpbmp.deleteOnExit()
    val retval = ProcessBuilder().inheritIO().command(
        "ffmpeg",
        "-i", fname,
        "-s", "8192x8192",
        "-q:v", "1",
        "-y", tmpbmp.absolutePath
    ).start().waitFor()
    System.err.println("ffmpeg returns $retval")
    val gray16beRasterCursor: Cursor = gray16bePixelCursor(tmpbmp)
    showsample(gray16beRasterCursor)
    showsample(gray16beRasterCursor)
    showsample(gray16beRasterCursor)
    showsample(gray16beRasterCursor)
    showsample(gray16beRasterCursor)
    showsample(gray16beRasterCursor)

    System.err.println(tmpbmp)
}


private fun showsample(c: Cursor) {
    val row = Random.nextInt(c.size)
    val xwidth = c.at(0).size
    val sampleWidth = min(80, xwidth / 2)
    val stripe = Random.nextInt(xwidth - sampleWidth)
    val sample =
        c[row].let {
            DoubleArray(sampleWidth) { i -> it[i].first as Double }
        }
    System.err.println("val from $row, $stripe..$sampleWidth:\n" + sample.toList().toString())
}
