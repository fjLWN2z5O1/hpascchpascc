import cursors.at
import cursors.context.TokenizedRow
import cursors.get
import cursors.io.left
import cursors.trieOnColumns
import org.junit.Test
import vec.macros.get
import java.nio.file.Paths

class ImgMetaTest {
    @Test
    fun testImgSizeCsv() {
        val dir = Paths.get("src/test/resources")
        val curs = TokenizedRow.CsvArraysCursor(dir.resolve("sizes.csv").toFile().readLines())
        val arrayMapOnColumns = curs.trieOnColumns("uuid")

        fun arraySizeDebug(uuid: String) = arrayMapOnColumns.get(uuid)!!.let {
            System.err.println("$uuid is at index " + it)
            (curs["size"] at it).left[0].let {
                System.err.println("image size is " + it)

            }
        }


        listOf(
            "0040581b-f1f2-4fbe-b043-b6bfea5404bb",
            "ffeae6f0-bbc9-11e8-b2bc-ac1f6b6435d0",
            "000a6c98-bb9b-11e8-b2b9-ac1f6b6435d0"
        ).forEach(::arraySizeDebug)

    }
}