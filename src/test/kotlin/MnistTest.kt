import cursors.context.TokenizedRow
import cursors.io.IOMemento
import cursors.io.writeISAM
import org.junit.Test
import vec.macros.*
import vec.util._l
import java.nio.file.Paths


class MnistTest {
    val dir = Paths.get("src/test/resources")
    val orig = _l["mnist_test", "mnist_train"]


    @Test
    fun binarize() {
        orig.forEach { s ->
            val resolve = dir.resolve(s+".csv")
            val csvArraysCursor = TokenizedRow.CsvArraysCursor(resolve.toFile().readLines() ,Vect0r(Int.MAX_VALUE) { ix: Int -> IOMemento.IoByte })
 csvArraysCursor.writeISAM(dir.resolve(s+".isam").toString(),)
        }
    }
}

